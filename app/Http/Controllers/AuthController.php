<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function input(){
        return view('album.input');
    }

    public function save(Request $request){
        $request->validate([
            'nama' => 'required',
            'penyanyi' => 'required',
            'harga' => 'required',
            'deskripsi' => 'required'
        ]);

        $query = DB::table('album')->insert([
            "nama" => $request["nama"],
            "penyanyi" => $request["penyanyi"],
            "harga" => $request["harga"],
            "deskripsi" => $request["deskripsi"]
        ]);

        return redirect('/home')->with('success', 'Data Album berhasil ditambahkan!');
    }

    public function regis(){
        return view('user.regis');
    }

    public function add1(){
        return view('pemasok.add');
    }
}
