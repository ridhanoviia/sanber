@extends('master')

@section('form')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <form class="form-horizontal">
                    <div class="card-body">
                        <h4 class="card-title">Personal Info</h4>
                        <div class="form-group row">
                            <label for="fname" class="col-sm-3 text-right control-label col-form-label">First Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="fname" placeholder="First Name Here">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="lname" class="col-sm-3 text-right control-label col-form-label">Last Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="lname" placeholder="Last Name Here">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="email" class="col-sm-3 text-right control-label col-form-label">Email</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="email" placeholder="Your Email Here">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="pass" class="col-sm-3 text-right control-label col-form-label">Password</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control" id="pass" placeholder="Password Here">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="phone" class="col-sm-3 text-right control-label col-form-label">No. Handphone</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="phone" placeholder="Contact No Here">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="alamat" class="col-sm-3 text-right control-label col-form-label">Alamat</label>
                            <div class="col-sm-9">
                                <textarea class="form-control"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="jk" class="col-sm-3 text-right control-label col-form-label">Jenis Kelamin</label>
                            <div class="col-md-9">
                                <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="Pria" name="radio-stacked" required>
                                    <label class="custom-control-label" for="Pria">Pria</label>
                                </div>
                                    <div class="custom-control custom-radio">
                                    <input type="radio" class="custom-control-input" id="Wanita" name="radio-stacked" required>
                                    <label class="custom-control-label" for="Wanita">Wanita</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="ttl" class="col-sm-3 text-right control-label col-form-label">TTL</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control date-inputmask" id="date-mask" placeholder="Enter Date">
                            </div>
                        </div>
                    </div>
                    <div class="border-top">
                        <div class="card-body">
                            <button type="button" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection       
            