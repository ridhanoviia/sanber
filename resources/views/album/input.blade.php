@extends('master')

@section('form')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <form class="form-horizontal">
                    <div class="card-body">
                        <h4 class="card-title">Personal Info</h4>
                        <div class="form-group row">
                            <label for="aname" class="col-sm-3 text-right control-label col-form-label">Album Name</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="aname" placeholder="Album Name Here">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="lname" class="col-sm-3 text-right control-label col-form-label">Penyanyi</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control" id="lname" placeholder="Penyanyi Name Here">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="pass" class="col-sm-3 text-right control-label col-form-label">Harga</label>
                            <div class="col-sm-9">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="..." aria-label="Recipient 's username" aria-describedby="basic-addon2">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="basic-addon2">Rp</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="alamat" class="col-sm-3 text-right control-label col-form-label">Deskripsi</label>
                            <div class="col-sm-9">
                                <textarea class="form-control"></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="border-top">
                        <div class="card-body">
                            <button type="button" class="btn btn-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>

        </div>
    </div>
@endsection       
            